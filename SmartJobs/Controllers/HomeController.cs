﻿using SmartJobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using System.Net.Http;
using System.Text;
using System.Threading;
namespace SmartJobs.Controllers
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        // This represents the view where the user will submit his/her resume and The jobs that the candidate wants to apply to...
        public IActionResult Index()
        {
            //delete session key soon
            return View();
        }

        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Process([Bind("query,zipcode")]JobSearch jobsearch, IFormFile resume)
        {
            
            if(resume == null || resume.Length ==0)
            {
                TempData["Error"] = "Please upload a copy of your resume";
                return RedirectToAction("Index");
            }
            
            
            // Figure out how to handle empty files
            // Regular expresion for correct format
            Match correctFormat = Regex.Match(resume.FileName, @"([a-zA-Z0-9\s_\\.\-\(\):])+(.doc|.docx)$",RegexOptions.IgnoreCase);
            
            // Validate the model
            if(ModelState.IsValid && correctFormat.Success)
            {
                // This is where the resume will be processed using openxml


                // the body of the resume will be stored here
                using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(resume.OpenReadStream(), false))
                {
                    Body b = wordprocessingDocument.MainDocumentPart.Document.Body;
                    string stringBody = b.InnerText;
                    jobsearch.resume = stringBody;

                }
                // make a post request here to the restful ap
                string key = "SSTFZ05I7GYVFVTV9Y4S7DCQOUQ0Y1VHNM6AOS2XHRUUCVG8A7JE15FH8KR3SFWVQ1HH92VD91LE7D7U8U2YTAFL5VAQBM6CMU7V";
                
                // endpoint stored here
                string endpoint = String.Format(@"https://smart-jobs.herokuapp.com/api/v1/bestjobs");
                 HerokuProcess process = new HerokuProcess();
               
                
                string json = JsonConvert.SerializeObject(jobsearch);
                StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = new HttpResponseMessage();
                string responseStr = String.Empty;
                using(HttpClient httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("X-Api-Key", key);
                    response = await httpClient.PostAsync(endpoint, content);
                    responseStr = await response.Content.ReadAsStringAsync();
                }
               
                process = JsonConvert.DeserializeObject<HerokuProcess>(responseStr);


                HttpContext.Session.SetString("pid", JsonConvert.SerializeObject(process));
                HttpContext.Session.SetString("jobsearch", JsonConvert.SerializeObject(jobsearch));



                return RedirectToAction("Output");
            }

            if (!correctFormat.Success)
            {
                TempData["Error"] = "File must be either doc or docx.";
            }


            
            return RedirectToAction("Index");
        }

        // Need to change the implementation of output to retrieve data from REST API
        public async Task<IActionResult> Output(){
            Console.WriteLine("Output statement hit");
            Console.WriteLine(HttpContext.Session.GetString("pid"));


            if(HttpContext.Session.GetString("pid") != null)
            {
                // retrieve process id here
                HerokuProcess hp = JsonConvert.DeserializeObject<HerokuProcess>(HttpContext.Session.GetString("pid"));
                // this needs to be retrieved from the endpoint
                List<Job> jobs = new List<Job>();
            
            
                // Here we will ping the status of the job results
                // We will ping for results every 15 seconds
                // while the status code is not 200, keep checking results every 15 seconds
                // if status code is 200 return the results
                string endpoint = String.Format(@"https://smart-jobs.herokuapp.com/api/v1/checkresults?pid={0}",hp.Process_id);
                string strResponse = String.Empty;
                using(HttpClient httpClient= new HttpClient())
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    do
                    {
                        response = await httpClient.GetAsync(endpoint);
                        Console.WriteLine("response = {0}",(int)response.StatusCode);
                        Thread.Sleep(15000);
                    } while ((int)response.StatusCode != 200);
                
                
                    strResponse = await response.Content.ReadAsStringAsync();
                    // line below throws error
                    jobs = JsonConvert.DeserializeObject<List<Job>>(strResponse);
                }

                JobSearch js = JsonConvert.DeserializeObject<JobSearch>(HttpContext.Session.GetString("jobsearch"));


                ViewData["query"] = js.query;
                ViewData["zipcode"] = js.zipcode;


                

                
                
                return View(jobs);
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Helper method to retrieve json data from heroku api
        /// </summary>
        /// <param name="endpoint"></param>
        /// <returns>List<Job> of jobs</returns>
        public static List<Job> getJobsFromJson(string endpoint)
        {

            // status 200 indicates request recieved and pid is returned
            // status 202 indicates that the jobs are still processing
            // the next status 200 indicates that the jobs have been successfully porcessed
            throw new NotImplementedException("method has not yet been implemented");
        }
        /// <summary>
        /// Pass in keyword missing from resume that will be used to generate a video search from YouTube with
        /// the first 10 results.
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        /// 
        [Route("/Home/LearningRecommendations",Name ="keyword")]
        public async Task<IActionResult> LearningRecommendations(string keyword)
        {
            
            #region Code for loading Youtube data
            // Snippet for url construction
            string query = String.Format("{0} tutorials", keyword);
            string apiKey = "AIzaSyCQ7t9jogvnuTb9tvD-bposCOQnRhm3j4M";
            string type = "video";
            string url = String.Format(@"https://www.googleapis.com/youtube/v3/search?part=snippet&key={0}&q={1}&maxResults={2}&type={3}", apiKey, query, 10, type);

            //Json data gets strored here
            String json = String.Empty;

            // HTTP request 
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            List<YouTubeVideo> ytVideos = new List<YouTubeVideo>();
            
            try{
                // HTTP response and code for reading the json data.
                using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    json = await streamReader.ReadToEndAsync();
                    streamReader.Close();
                }

                //Initialize new youtube data Object where json data will get deserialized to.
                YouTubeData youTubeData = JsonConvert.DeserializeObject<YouTubeData>(json);

                

                //iterate through items in YouTubeData object
                foreach(var video in youTubeData.items)
                {
                    // Retrieve video data
                    string videoId = video.id["videoId"];
                    string title = video.snippet.title;
                    string description = video.snippet.description;
                    string channelName = video.snippet.channelTitle;
                    DateTime datePublished = video.snippet.publishedAt;
                    string thumbNailUrl = video.snippet.thumbnails["high"].url;


                    //Store data in YouTubeVideo object
                    YouTubeVideo ytVideo = new YouTubeVideo()
                    {
                        VideoId = videoId,
                        Title = title,
                        Description = description,
                        ChannelName = channelName,
                        DatePublished = datePublished,
                        ThumbNailUrl = thumbNailUrl
                    };
                    ytVideos.Add(ytVideo);

                }
            }

            catch(Exception ex)
            {
                Console.WriteLine("Error: Failed to get Youtube Data");
                Console.WriteLine(ex.StackTrace);
                ytVideos = null;
            }


            #endregion
            #region code for loading learning reccomendations data from heroku service
            List<UdemyCourse> udemyCourses = new List<UdemyCourse>();
            try
            {
                query = keyword;
                url = String.Format(@"http://127.0.0.1:80/api/courses?search={0}", query);
                var request2 = (HttpWebRequest)WebRequest.Create(url);
                

                //request header for authorization here
                request2.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (HttpWebResponse response = (HttpWebResponse)await request2.GetResponseAsync())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    // Obtain JSON data
                    json = await streamReader.ReadToEndAsync();
                    streamReader.Close();
                    UdemyData udemyData = JsonConvert.DeserializeObject<UdemyData>(json);

                    foreach(var data in udemyData.results)
                    {
                        udemyCourses.Add(
                                new UdemyCourse
                                {
                                    CourseTitle = data.title,
                                    CourseImageUrl = data.image_480x270,
                                    CourseInstructors = data.visible_instructors,
                                    CourseUrl = data.url,
                                    CoursePrice =data.price
                                }
                            );
                    }
                };
            }
            catch(Exception ex)
            {
                // Error message
                Console.WriteLine("Failed to retrieve results");
                Console.WriteLine(ex.StackTrace);
                udemyCourses = null;

            }

            


            LearningRecommendations recommendations = new LearningRecommendations()
            {
                UdemyCourses = udemyCourses,
                YouTubeVideos = ytVideos,
                
            };



                #endregion



                return View(recommendations );



        }
    }
}
