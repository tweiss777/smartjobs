﻿using System;

namespace SmartJobs.Models
{
    public class YouTubeVideo
    {
        public string VideoId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ChannelName { get; set; }
        public DateTime DatePublished { get; set; }
        public string ThumbNailUrl { get; set; }

    }
}
