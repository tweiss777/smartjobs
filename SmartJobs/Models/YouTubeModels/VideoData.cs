﻿using System;
using System.Collections.Generic;

namespace SmartJobs.Models
{
    public class VideoData
    {
        public DateTime publishedAt { get; set; }
        public string channelId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public Dictionary<string, ThumbNail> thumbnails { get; set; }
        public string channelTitle { get; set; }
        public string liveBroadcastContent { get; set; }
    }
}
