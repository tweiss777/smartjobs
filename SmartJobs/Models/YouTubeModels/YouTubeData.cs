﻿using System;
using System.Collections.Generic;

namespace SmartJobs.Models
{
    // The main model for the youtube api
    public class YouTubeData
    {
        public string kind {get;set;}
        public string etag {get;set;}
        public string nextPageToken {get;set;}
        public string regionCode {get;set;}
        public Dictionary <string,int> pageInfo {get;set;}

        // Field that gets iterated over
        public IList<Item> items {get;set;}
            
    }
}
