using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace SmartJobs.Models
{
    public class Job
    {
        public String Title{get;set;}
        public String Description{get;set;}
        public List<String> Matching_keywords{get;set;}
        public List<String> Missing_keywords{get;set;}
        public String JobUrl { get; set; }
    }
}