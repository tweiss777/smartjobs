﻿using System;
using System.Collections.Generic;

namespace SmartJobs.Models
{
    public class Item
    {
        public string kind { get; set; }
        public string etag { get; set; }
        // Retrieve videoID
        public Dictionary<string, string> id { get; set; }
        public VideoData snippet { get; set; }
    }
}
