using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace SmartJobs.Models
{
    public class JobSearch
    {
        [Required(ErrorMessage ="Job search is required."),Display(Name="Key word or job search")]
        public string query{get; set;}
        
        [Display(Name="Zip code"),Required(ErrorMessage = "Invalid zip code"), RegularExpression("[0-9]{5}")]
        public string zipcode{get; set;}

        public string resume { get; set; }



    }



}