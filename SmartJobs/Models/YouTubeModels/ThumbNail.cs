﻿using System;
namespace SmartJobs.Models
{
    
    public class ThumbNail
    {
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }
}
