using System;
namespace SmartJobs.Models
{
    public class PriceDetail
    {
       public string price_string {get;set;}
       public float amount {get;set;}
       public string currency {get;set;}
       public string currency_symbol {get;set;}
        
    }
}