using System;
using System.Collections.Generic;

namespace SmartJobs.Models
{
    public class UdemyCourse
    {
    public string CourseTitle {get;set;}
    public string CourseUrl {get;set;}
    public string CoursePrice {get;set;}

    // Use this field to get the instructor names as well as their photos.
    public List<Instructor> CourseInstructors {get;set;}
    public String CourseImageUrl {get;set;}

    }
}