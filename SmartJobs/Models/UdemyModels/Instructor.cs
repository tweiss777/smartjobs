using System;
namespace SmartJobs.Models
{
    // This class represents information about the instructor
    public class Instructor 
    {
        public string image_100x100 {get;set;}
        public string job_title {get;set;}
        public string name {get;set;}
        public string title {get;set;}
        public string image_50x50 {get;set;}
        public string _class {get;set;}
        public string display_name {get;set;}
        public string initials {get;set;}
        public string url {get;set;}

    }
}