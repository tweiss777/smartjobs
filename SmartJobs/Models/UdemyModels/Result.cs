using System;
using System.Collections.Generic;
namespace SmartJobs.Models
{
    public class Result
    {
        public string _class {get;set;}
        public int id {get;set;}
        public string title {get;set;}
        public string url {get;set;}
        public bool is_paid {get;set;}
        public string price {get;set;}
        public PriceDetail price_detail {get;set;}
        public List<Instructor> visible_instructors {get;set;}
        public string image_125_H {get; set;}
        public string image_240x135 {get;set;}
        public bool is_practice_test_course {get;set;}
        public string image_480x270 {get;set;}
        public string published_title {get;set;}
        public object predictive_score{get;set;}
        public float relevancy_score {get;set;}
        public object input_features {get;set;}
        public object lecture_search_result {get;set;}
        public List<object> curriculum_lectures {get;set;}
        public object order_in_results {get;set;}




    }
}