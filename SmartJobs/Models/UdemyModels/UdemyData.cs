using System;
using System.Collections.Generic;
namespace SmartJobs.Models
{
    // The main model of udemy api
    public class UdemyData
    {
        public int count {get; set;}
        
        // the next field is the next page in the search 
        public string next {get;set;} 

        // The previous field is the previous page in the search
        public string previous {get;set;}
        public List<Result> results {get;set;}
        public List<object> aggregations {get;set;}
        public Suggestion suggestion {get;set;}



    }
}