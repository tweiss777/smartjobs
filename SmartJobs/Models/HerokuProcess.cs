// Model representing the worker 
// holds the process id and the message from the server associated with the request

namespace SmartJobs.Models
{
    public class HerokuProcess
    {
        // process id used to reference the request
        private string process_id;

        // message from the server
        private string message;

        public string Message { get => message; set => message = value; }
        public string Process_id { get => process_id; set => process_id = value; }
    }
}


