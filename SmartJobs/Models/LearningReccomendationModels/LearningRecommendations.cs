using System;
using System.Collections.Generic;

namespace SmartJobs.Models
{
    public class LearningRecommendations
    {
        public IList<UdemyCourse> UdemyCourses {get;set;}
        public IList<YouTubeVideo> YouTubeVideos {get;set;}
    }
}